# Tarea1Git

Tarea 1 para el curso de Análisis y Diseño de Sistemas 2.
1 Semestre 2,020

Pasos para la tarea:
  1. Clonar el repositorio  (Getting Started)
  2. Crear rama   /feature/t1_Carné   --> /feature/t1_202011111
  3. Agregar Cambios en src/app/app.component.html
```bash
    <tr>
        <td> Carne </td>   //Carné
        <td> Nombre </td>  //Nombre 
        <td> correo </td>  //Correo
    </tr>
```
  4. Subir los cambios 
       - git add, git commit, git push 
  5. Realizar Merge Request (Crear cuenta Gitlab, pedir acceso al repositorio y crear Merge Request)
  6. Verificar Resultados
       http://34.69.120.142 


## Getting Started

[Fork](https://docs.gitlab.com/ee/workflow/forking_workflow.html#creating-a-fork) this repository using the gitlab mechanism or you can clone the entire repository as follows:

```bash
git clone git@gitlab.com:ayd2_1s_2020/tarea1-git.git

git clone https://gitlab.com/ayd2_1s_2020/tarea1-git.git

cd tarea1-git
# install the project's dependencies
$ npm install
# watches your files and uses livereload by default run `npm start` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.
$ npm start
# prod build, will output the production application in `dist`
# the produced code can be deployed (rsynced) to a remote server
$ npm run build
```

### Tools

The main tools used in this pipeline are:

- GitLab: This is the main CI/CD tool in the Rosetta platform, it will handle Git repositories and continuous integration and deployment for each team.
- NodeJs: (requires node.js version >= 0.8.4)
    This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.19.

Those tools will be applied in the areas:
- CI/CD Pipeline
- Test Automation



## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).


## Author

* **Dodany Girón**      - dodanygiron@gmail.com 
Análisis y DIseño de sistemas 2
USAC